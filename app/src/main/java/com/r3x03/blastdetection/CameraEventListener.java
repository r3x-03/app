package com.r3x03.blastdetection;

import android.graphics.Bitmap;

public interface CameraEventListener {
    public void onPhotoCaptured(Bitmap bitmap);
}
