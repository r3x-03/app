package com.r3x03.blastdetection;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.Image;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;


public class CameraView extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    final String TAG = "CameraView";

    private Context context;
    private CameraEventListener cameraEventListener;
    public CameraView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.context = context;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    public boolean isCameraEventListenerSet(){
        return cameraEventListener != null;
    }
    public void setCameraEventListener(CameraEventListener cameraEventListener){
        this.cameraEventListener = cameraEventListener;
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    public void surfaceCreated(SurfaceHolder holder) {
        startCamera(holder);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        stopCamera();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    }

    private int rotation;
    private void startCamera(SurfaceHolder holder){
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera = getCameraInstance();
            if(mCamera == null){
                //TODO: error
            }
            mCamera.setPreviewDisplay(holder);

            rotation = determineCameraRotation(0);
            mCamera.setDisplayOrientation(rotation);

            Camera.Parameters params = mCamera.getParameters();

            String focus_mode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;

            // continuous autofocus
            if(params.getSupportedFocusModes().contains(focus_mode)) {
                params.setFocusMode(focus_mode);
            }

            // exposure
            if(exposure != 0){
                params.setExposureCompensation(exposure);
            }

            // white balance
            if(wb_mode != null){
                params.setWhiteBalance(wb_mode);
            }

            // flash
            if(flash_mode != null){
                params.setFlashMode(flash_mode);
            }

            List<Camera.Size> pictureSizes = params.getSupportedPictureSizes();
            int index = getBestSize(pictureSizes, 0);
            params.setPictureSize(pictureSizes.get(index).width, pictureSizes.get(index).height);

            float aspectRatio = (float)pictureSizes.get(index).width / (float)pictureSizes.get(index).height;

            List<Camera.Size> previewSizes = params.getSupportedPreviewSizes();
            index = getBestSize(previewSizes, aspectRatio);
            params.setPreviewSize(previewSizes.get(index).width, previewSizes.get(index).height);;


            mCamera.setParameters(params);
            mCamera.startPreview();

            if(rotation % 180 == 90){
                mRatioWidth = params.getPreviewSize().height;
                mRatioHeight = params.getPreviewSize().width;
            } else{
                mRatioWidth = params.getPreviewSize().width;
                mRatioHeight = params.getPreviewSize().height;
            }
            requestLayout();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        ImageView frame = ((Activity) context).findViewById(R.id.camera_frame);
        ViewGroup.LayoutParams params = frame.getLayoutParams();
        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height * mRatioWidth / mRatioHeight) {
                setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
                params.width = min(width, width * mRatioHeight / mRatioWidth);
            } else {
                setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
                params.width = min(height * mRatioWidth / mRatioHeight, height);
            }
            params.height = (int)( params.width * (1920.f/1080)); //make the image fit
            frame.setLayoutParams(params);
        }
    }
    
    public void stopCamera(){
        try {
            mCamera.stopPreview();
            mCamera.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int min(int a, int b){
        if(a < b) return a;
        else return b;
    }
    private int getBestSize(List<Camera.Size> sizes, float aspect){
        int max = 0;
        int index = 0;

        for (int i = 0; i < sizes.size(); i++){
            Camera.Size s = sizes.get(i);
            int size = s.height * s.width;

            if(aspect != 0 && (float)s.width / (float)s.height != aspect){
                continue;
            }
            if (size > max) {
                index = i;
                max = size;
            }
        }

        return index;
    }

    private int determineCameraRotation(int cameraId) {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    private int exposure = 0;
    public int getExposureSteps(){
        Camera.Parameters params = mCamera.getParameters();
        return  (params.getMaxExposureCompensation() - params.getMinExposureCompensation() + 1);
    }
    public int getExposure(){
        Camera.Parameters params = mCamera.getParameters();
        return exposure - params.getMinExposureCompensation();
    }
    public void setExposure(int i){
        try{
            Camera.Parameters parameters = mCamera.getParameters();
            int minimum = parameters.getMinExposureCompensation();
            parameters.setExposureCompensation(minimum + i);
            mCamera.setParameters(parameters);
            exposure = minimum + i;
        } catch (Exception e){ }
    }

    private String wb_mode = null;
    public String switchWhiteBalance(){
        try{
            Camera.Parameters parameters = mCamera.getParameters();

            String currentWB = parameters.getWhiteBalance();
            List<String> supportedWbModes = parameters.getSupportedWhiteBalance();

            // some devices dont support white balance
            if(supportedWbModes == null || supportedWbModes.size() == 0) return Camera.Parameters.WHITE_BALANCE_AUTO;

            int next = (supportedWbModes.indexOf(currentWB) + 1) % supportedWbModes.size();

            parameters.setWhiteBalance(supportedWbModes.get(next));
            mCamera.setParameters(parameters);

            wb_mode = supportedWbModes.get(next);
            return supportedWbModes.get(next);
        } catch (Exception e){
            wb_mode = null;
            return Camera.Parameters.WHITE_BALANCE_AUTO;
        }
    }

    private String flash_mode = null;
    public String getFlashMode(){
        Camera.Parameters parameters = mCamera.getParameters();
        return parameters.getFlashMode();
    }
    public void setFlashMode(String flash_mode){
        try{
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setFlashMode(flash_mode);
            mCamera.setParameters(parameters);

            this.flash_mode = flash_mode;
        } catch (Exception e){ }
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Bitmap capturedImage = BitmapFactory.decodeByteArray(data, 0, data.length);

            //stop camera and load neural net
            stopCamera();

            // rotation matrix for image
            Matrix rotationMatrix = new Matrix();
            rotationMatrix.postRotate(rotation);

            // cropping algorithm
            // let w be 996/1080 of the image's shortest side
            // take a center crop of size w x w
            // then resize to 512x512

            int w = min(capturedImage.getWidth(), capturedImage.getHeight()) * 996 / 1080;

            int centerX = capturedImage.getWidth() / 2;
            int centerY = capturedImage.getHeight() / 2;
            int radius = w/2;

            // crop and rotate
            capturedImage = Bitmap.createBitmap(capturedImage, centerX - radius, centerY - radius, w, w, rotationMatrix, true);
            if(isCameraEventListenerSet()){
                cameraEventListener.onPhotoCaptured(capturedImage);
            }

            startCamera(mHolder);
        }
    };

    public void capture() {
        if(mCamera != null){ // avoid null pointer exception
            mCamera.takePicture(null, null, null, mPicture);
        } else{
            Toast.makeText(context, "Device camera not running", Toast.LENGTH_SHORT).show();
        }
    }
}
