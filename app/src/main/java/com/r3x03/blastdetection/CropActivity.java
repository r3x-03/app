/*
 * Copyright (c) 2015 Naver Corp.
 * Copyright (c) 2021 R3X-03.
 * @Author Ohkyun Kim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.r3x03.blastdetection;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;

import com.naver.android.helloyako.imagecrop.model.ViewState;
import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.IOException;

public class CropActivity extends Activity {
    public static final String TAG = "CropActivity";

    private ImageCropView imageCropView;
    private ViewState viewState;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_acitvity);
        imageCropView = findViewById(R.id.cropUI);

        Intent i = getIntent();
        Uri uri = i.getData();

//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        int imageWidth = (int) ( (float) metrics.widthPixels / 1.5 );
//        int imageHeight = (int) ( (float) metrics.heightPixels / 1.5 );
//
//        bitmap = BitmapLoadUtils.decode(uri.toString(), imageWidth, imageHeight);
//
//        imageCropView.setImageBitmap(bitmap);

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            imageCropView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }


        imageCropView.setAspectRatio(1, 1);
    }

    public void done(View v){
        Bitmap croppedBitmap = imageCropView.getCroppedImage();
        Global.sharedBitmap = croppedBitmap.copy(croppedBitmap.getConfig(), croppedBitmap.isMutable());
        setResult(RESULT_OK);
        finish();
    }
}