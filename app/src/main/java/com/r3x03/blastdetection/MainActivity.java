package com.r3x03.blastdetection;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity {

    final int CODE_REQUEST_CAMERA_AND_STORAGE  = 1;
    final String PERMISSIONS_CAMERA_AND_STORAGE[] = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private CameraView cameraView;
    private NeuralNetwork nn;
    private WebView helpWebView;
    private WebView resultsWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!EasyPermissions.hasPermissions(this, PERMISSIONS_CAMERA_AND_STORAGE)){
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_camera),
                    CODE_REQUEST_CAMERA_AND_STORAGE,
                    PERMISSIONS_CAMERA_AND_STORAGE);
        }

        nn = new NeuralNetwork(this);

        loadCamera();
    }



    // load camera if and only if we have camera permission
    @AfterPermissionGranted(CODE_REQUEST_CAMERA_AND_STORAGE)
    private void loadCamera() {
        setContentView(R.layout.camera_layout);
    }

    // this is a hack to set the callback if it hasn't been set
    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);

        CameraView cameraView = findViewById(R.id.camera_preview);
        if(cameraView == null) return;

        if(cameraView.isCameraEventListenerSet() == false){
            cameraView.setCameraEventListener(new CameraEventListener() {
                @Override
                public void onPhotoCaptured(final Bitmap capturedImage) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            showResults(capturedImage);
                        }
                    });
                }
            });
        }

        helpWebView = (WebView) findViewById(R.id.helpWebView);
        resultsWebView = (WebView) findViewById(R.id.resultsWebView);

    }

    @Override
    public void onBackPressed() {
        if (helpWebView.canGoBack()){
            helpWebView.goBack();
        } else{
            helpWebView.setVisibility(View.GONE);
        }

        if (resultsWebView.canGoBack()){
            resultsWebView.goBack();
        } else{
            resultsWebView.setVisibility(View.GONE);
        }
    }

    public void helpClicked(View view){
        helpWebView.setWebViewClient(new WebViewClient());
        helpWebView.loadUrl("file:///android_asset/Help Screen HTML/Help Screen HTML.html");
        helpWebView.getSettings().setJavaScriptEnabled(true);
        if(helpWebView.getVisibility() == View.GONE){
            helpWebView.setVisibility(View.VISIBLE);
        } else {
            helpWebView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();

        /*
            HACK: fix elements like exposure slider, help menu, results menu not appearing after resuming app
         */
        findViewById(R.id.frameLayout).requestLayout();
    }

    private int getWhiteBalanceIcon(String wb_mode){
        if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_AUTO))                  return R.drawable.ic_wb_auto;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_CLOUDY_DAYLIGHT))  return R.drawable.ic_wb_cloudy;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_INCANDESCENT))     return R.drawable.ic_wb_incandescent;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_FLUORESCENT))      return R.drawable.ic_wb_iridescent;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_WARM_FLUORESCENT)) return R.drawable.ic_wb_iridescent;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_SHADE))            return R.drawable.ic_wb_shade;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_DAYLIGHT))         return R.drawable.ic_wb_sunny;
        else if(wb_mode.equals(Camera.Parameters.WHITE_BALANCE_TWILIGHT))         return R.drawable.ic_wb_twilight;
        else return R.drawable.ic_wb_auto; // other possible white balance modes
    }

    public void switchExposure(View view){
        SeekBar seekBar = findViewById(R.id.evSlider);

        if(seekBar.getVisibility() == View.VISIBLE){
            seekBar.setVisibility(View.GONE);
        } else{
            seekBar.setVisibility(View.VISIBLE);

            CameraView cameraView = findViewById(R.id.camera_preview);

            seekBar.setMax(cameraView.getExposureSteps() - 1);
            seekBar.setProgress(cameraView.getExposure());
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    CameraView cameraView = findViewById(R.id.camera_preview);
                    cameraView.setExposure(i);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) { }
            });
        }
        }


    public void switchWhiteBalance(View view){
        ImageButton wbButton = findViewById(R.id.wbButton);
        CameraView cameraView = findViewById(R.id.camera_preview);

        String newWhiteBalance = cameraView.switchWhiteBalance();
        wbButton.setImageResource( getWhiteBalanceIcon(newWhiteBalance) );
    }

    public void switchFlashMode(View view){
        ImageButton flashButton = findViewById(R.id.flashButton);
        CameraView cameraView = findViewById(R.id.camera_preview);

        if(cameraView.getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF)){
            cameraView.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            flashButton.setImageResource( R.drawable.ic_flash_on );
        } else{
            cameraView.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            flashButton.setImageResource( R.drawable.ic_flash_off );
        }

    }
    // camera capture
    public void capture(View view) {
        cameraView = findViewById(R.id.camera_preview);



        if(cameraView != null){ // avoid null pointer exception
            cameraView.capture();
        } else{
            Toast.makeText(this, "Camera UI not running", Toast.LENGTH_SHORT).show();
        }
    }

    static final int REQUEST_IMAGE_GET = 1;
    static final int REQUEST_IMAGE_CROP = 2;

    public void selectImage(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GET);
        }

    }

    private void getResults(Bitmap capturedImage){
        float[] predictions = nn.predictionFromBitmap(capturedImage);
        Toast.makeText(this, "Healthy " + predictions[0] + "Infected " + predictions[1], Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK) {
            Bitmap thumbnail = data.getParcelableExtra("data");
            Uri fullPhotoUri = data.getData();

            // Do work with photo saved at fullPhotoUri
            Intent intent = new Intent(this, CropActivity.class);
            intent.setData(fullPhotoUri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_IMAGE_CROP);
            }
        } else if(requestCode == REQUEST_IMAGE_CROP && resultCode == RESULT_OK){
            // crop activity dumps final image to sharedBitmap
            showResults(Global.sharedBitmap);
        }
    }


    private void showResults(final Bitmap capturedImage) {
        resultsWebView.setVisibility(View.VISIBLE);

        resultsWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                float[] predictions = nn.predictionFromBitmap(capturedImage);

                resultsWebView.loadUrl("javascript:(function() {" +
                        "var parent = document.getElementById('paragraph');" +
                        "parent.innerHTML = '" + "Healthy " + predictions[0] + "Infected " + predictions[1] + "';" +
                        "})()");
                super.onPageFinished(view, url);
            }
        });

        resultsWebView.loadUrl("file:///android_asset/Results Screen HTML/Results Screen HTML.html");
        resultsWebView.getSettings().setJavaScriptEnabled(true);
    }



}