package com.r3x03.blastdetection;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Color;

import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.CompatibilityList;
import org.tensorflow.lite.gpu.GpuDelegate;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class NeuralNetwork {
    private Interpreter interpreter;
    private final String MODEL_FILE = "model.tflite";
    private final int IMAGE_WIDTH = 512;
    private final int NUM_CLASSES = 2;
    public NeuralNetwork(Context context){
        // Initialize interpreter with GPU delegate
        Interpreter.Options options = new Interpreter.Options();

        try {
            CompatibilityList compatList = new CompatibilityList();

            if(compatList.isDelegateSupportedOnThisDevice()){
                // if the device has a supported GPU, add the GPU delegate
                GpuDelegate.Options delegateOptions = compatList.getBestOptionsForThisDevice();
                GpuDelegate gpuDelegate = new GpuDelegate(delegateOptions);
                options.addDelegate(gpuDelegate);
            } else {
                // if the GPU is not supported, run on 4 threads
                options.setNumThreads(4);
            }
        } catch (ExceptionInInitializerError e) { // android version does not support GPU acceleration
            // if the GPU is not supported, run on 4 threads
            options.setNumThreads(4);
        }


        try {
            interpreter = new Interpreter(loadModelFile(context), options);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MappedByteBuffer loadModelFile(Context context) throws IOException {
        AssetFileDescriptor fileDescriptor = context.getAssets().openFd(MODEL_FILE);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    float[] predictionFromBitmap(Bitmap bitmap){
        // force nearest neighbor as model uses nearest neighbor
        Bitmap resized = bitmap.createScaledBitmap(bitmap, IMAGE_WIDTH, IMAGE_WIDTH, false);
        float imageForTFLite[][][][] = new float[1][IMAGE_WIDTH][IMAGE_WIDTH][3];
        for(int y=0; y<IMAGE_WIDTH; y++){
            for(int x=0; x<IMAGE_WIDTH; x++){
                int pixel = resized.getPixel(x, y);

                imageForTFLite[0][y][x][0] = Color.blue(pixel) - 103.939f;
                imageForTFLite[0][y][x][1] = Color.green(pixel) - 116.779f;
                imageForTFLite[0][y][x][2] = Color.red(pixel) - 123.68f;
            }
        }

        float output[][] = new float[1][NUM_CLASSES];
        interpreter.run(imageForTFLite, output);
        return output[0];
    }
}
